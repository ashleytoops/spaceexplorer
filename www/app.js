(function(angular) {
  'use strict';
  angular.module('spaces', []).controller('SpaceExplorer', ['$scope', '$http', function($scope, $http) {
    var baseUrl = 'https://22920309-6c6a-4821-b1c6-af83a919b7ef.mock.pstmn.io';
    angular.extend($scope, {
    	created: {},
    	updated: {},
      initialize: function() {
        $scope.showEntry = true;
        $http.get(baseUrl + '/space').then(function(space) {
          $scope.spaces = space.data.items;
          $scope.selectSpace($scope.spaces[0]);
        });
        $scope.getUsers();
      },
      getUsers: function() {
        $http.get(baseUrl + '/users').then(function(data) {
          $scope.users = data.data.items;
        });
      },
      getUser: function(createdBy, updatedBy, entryId) {
        console.log("in here");
        $http.get(baseUrl + '/users/' + createdBy).then(function(created) {
          $scope.created[entryId] = created.data.fields.name;
          $http.get(baseUrl + '/users/' + updatedBy).then(function(updated) {
            $scope.updated[entryId] = updated.data.fields.name;
          });
        });
      },
      getEntries: function(spaceId) {
        $http.get(baseUrl + '/space/' + spaceId + '/entries').then(function(entry) {
          $scope.entries = entry.data.items;
          for (var x in $scope.entries) {
          	$scope.getUser($scope.entries[x].sys.createdBy, $scope.entries[x].sys.updatedBy, $scope.entries[x].sys.id);
          }
        });
      },
      getAssets: function(spaceId) {
        $http.get(baseUrl + '/space/' + $scope.currentSpace.sys.id + '/assets').then(function(asset) {
          $scope.assets = asset.data.items;
          for (var x in $scope.assets) {
          	$scope.getUser($scope.assets[x].sys.createdBy, $scope.assets[x].sys.updatedBy, $scope.assets[x].sys.id);
          }
        });
      },
      selectSpace: function(space) {
        $scope.currentSpace = space;
        $http.get(baseUrl + '/users/' + space.sys.createdBy).then(function(user) {
          $scope.createdBy = user.data.fields.name;
        });
        $scope.getEntries(space.sys.id);
        $scope.getAssets(space.sys.id);
      },
      selectTab: function(tab) {
        if (tab === 'entry')
          $scope.showEntry = true;
        else
          $scope.showEntry = false;
      },
      sortBy: function(type) {
        $scope.reverse = ($scope.propertyName === type) ? !$scope.reverse : false;
        $scope.propertyName = type;
      }
    });
    $scope.initialize();
  }]);
})(window.angular);
